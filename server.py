import socket
from threading import Thread

host = 'localhost'
port = 5555

users = []
nicknames = []

#socket initialization
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen()

#sends message to everyone who's connected
def broadcast(msg):
    for user in users:
        user.send(msg)

#receive user msg, broadcast it to others, remove users
def handle_user(user):
    while True:
        try:
            msg = user.recv(1024)
            broadcast(msg)
        except:
            user_index = users.index(user)
            users.remove(user)
            user.close()
            nickname = nicknames[user_index]
            broadcast("{} has disconnected.".format(nickname))
            nicknames.remove(nickname)
            break

#wait for connections, start new thread when connected
def accept_new_connections():
    while True:
        try:
            user, user_address = server.accept()
            print("{} has connected!".format(user_address))
            user.send('NICK'.encode('ascii'))
            nickname = user.recv(1024).decode('ascii')
            nicknames.append(nickname)
            users.append(user)
            broadcast("{} has joined!".format(nickname))
            Thread(target=handle_user, args=(user,)).start()
        except:
            print("fail")
            break




